# maven

This project contains Dockerfile to build maven imaged with cached dependencies. The main idea is to keep 99% required to build a project jars inside a maven docker image. In this case we don't need to download such jars from maven-central or other nexus repo.

```yaml
build-docker-image:
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2
  services:
    - docker:dind
  stage: build
  image: "docker:latest"
  script:
    - wget -O Dockerfile https://gitlab.com/alexej.vlasov/maven/raw/master/Dockerfile
    - cp -r syncer src
    - docker build --rm -t build/syncer-maven:latest .
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $CI_REGISTRY_IMAGE/build/syncer-maven:latest .
    - docker push $CI_REGISTRY_IMAGE/build/syncer-maven:latest
  when: manual
```

      